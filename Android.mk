LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := TifWithExoPlayer
LOCAL_MODULE_TAGS := TifWithExoPlayer

# Only compile source java files in this apk.
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_SRC_FILES += $(call all-Iaidl-files-under, src)
LOCAL_JAVA_LIBRARIES := android.test.runner
LOCAL_PROGUARD_ENABLED := disabled

LOCAL_DEX_PREOPT := false

LOCAL_CERTIFICATE := platform

LOCAL_SDK_VERSION := current

LOCAL_STATIC_JAVA_LIBRARIES := \
    exoplayer \
    appcompat-v4-local \
    appcompat-v7-local \
    android-support-v7-recyclerview-local \
    android-support-v17-leanback-local \

LOCAL_PRIVILEGED_MODULE := true


LOCAL_RESOURCE_DIR += \
    $(LOCAL_PATH)/../leanback/res \
    $(LOCAL_PATH)/../recyclerview/res \
    $(LOCAL_PATH)/../appcompat/res \
    $(LOCAL_PATH)/res \

LOCAL_AAPT_FLAGS := --auto-add-overlay \
    --extra-packages android.support.v7.recyclerview \
    --extra-packages android.support.v17.leanback \
    --version-name "$(version_name_package)" \

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := \
    exoplayer:./libs/exoplayer_r1.2.4.jar \
    appcompat-v4-local:../appcompat/libs/android-support-v4.jar \
    appcompat-v7-local:../appcompat/libs/android-support-v7-appcompat.jar \
    android-support-v7-recyclerview-local:../recyclerview/libs/android-support-v7-recyclerview.jar \
    android-support-v17-leanback-local:../leanback/libs/android-support-v17-leanback.jar \

include $(BUILD_MULTI_PREBUILT)
