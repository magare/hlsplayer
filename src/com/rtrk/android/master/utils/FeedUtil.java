
package com.rtrk.android.master.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import com.rtrk.android.master.R;
import com.rtrk.android.master.player.BasicTvInputService;
import com.rtrk.android.master.xmltv.XmlTvParser;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

/**
 * Static helper methods for fetching the channel feed.
 */
public class FeedUtil {
    private static final String TAG = "FeedUtil";
    private static XmlTvParser.TvListing sTvListing;

    private static final int URLCONNECTION_CONNECTION_TIMEOUT_MS = 3000; // 3
                                                                         // sec
    private static final int URLCONNECTION_READ_TIMEOUT_MS = 10000; // 10 sec

    private FeedUtil() {
    }

    public static XmlTvParser.TvListing getTvListings(Context context, String inputId) {
        /* parse XML at res/raw/basic_tv_input_sources.xml */
        Uri catalogUri = Uri.parse("android.resource://" + context.getPackageName() + "/"
                + R.raw.basic_tv_input_sources);
        Log.d(TAG, catalogUri.toString());

        try (InputStream inputStream = getInputStream(context, catalogUri)) {
            sTvListing = XmlTvParser.parse(inputStream);
        } catch (IOException e) {
            Log.e(TAG, "Error in fetching " + catalogUri, e);
        }
        return sTvListing; /* return all tv info discovered in xml */
    }

    public static InputStream getInputStream(Context context, Uri uri) throws IOException {
        InputStream inputStream;
        if (ContentResolver.SCHEME_ANDROID_RESOURCE.equals(uri.getScheme())
                || ContentResolver.SCHEME_ANDROID_RESOURCE.equals(uri.getScheme())
                || ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            inputStream = context.getContentResolver().openInputStream(uri);
        } else {
            URLConnection urlConnection = new URL(uri.toString()).openConnection();
            urlConnection.setConnectTimeout(URLCONNECTION_CONNECTION_TIMEOUT_MS);
            urlConnection.setReadTimeout(URLCONNECTION_READ_TIMEOUT_MS);
            inputStream = urlConnection.getInputStream();
        }
        return new BufferedInputStream(inputStream);
    }
}
