
package com.rtrk.android.master.utils;

import android.media.tv.TvContentRating;

public final class PlaybackInfo {
    
    public final long startTimeMs;
    public final long endTimeMs;
    public final String videoUrl;
    public final int videoType;
    public final TvContentRating[] contentRatings;

    public PlaybackInfo(long startTimeMs, long endTimeMs, String videoUrl, int videoType,
            TvContentRating[] contentRatings) {
        this.startTimeMs = startTimeMs;
        this.endTimeMs = endTimeMs;
        this.contentRatings = contentRatings;
        this.videoUrl = videoUrl;
        this.videoType = videoType;
    }
}
