package com.rtrk.android.master.activity;

public class Monitor {
	private int numSignal;

	public synchronized void reset() {
		numSignal = 0;
	}

	public synchronized void signal() {
		numSignal++;
		notifyAll();
	}

	public synchronized boolean waitForSignal() throws InterruptedException {
		return waitForCountedSignals(1) > 0;
	}

	public synchronized int waitForCountedSignals(int targetCount)
			throws InterruptedException {
		while (numSignal < targetCount) {
			wait();
		}
		return numSignal;
	}

	public synchronized boolean waitForSignal(long timeoutMs)
			throws InterruptedException {
		return waitForCountedSignals(1, timeoutMs) > 0;
	}

	public synchronized int waitForCountedSignals(int targetCount,
			long timeoutMs) throws InterruptedException {
		if (timeoutMs == 0) {
			return waitForCountedSignals(targetCount);
		}
		long deadline = System.currentTimeMillis() + timeoutMs;
		while (numSignal < targetCount) {
			long delay = deadline - System.currentTimeMillis();
			if (delay <= 0) {
				break;
			}
			wait(delay);
		}
		return numSignal;
	}

	public synchronized boolean isSignalled() {
		return numSignal >= 1;
	}

	public synchronized int getNumSignal() {
		return numSignal;
	}
}
