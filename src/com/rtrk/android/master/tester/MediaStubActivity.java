package com.rtrk.android.master.tester;

import android.app.Activity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Surface;

import com.rtrk.android.master.R;

public class MediaStubActivity extends Activity{
    private SurfaceHolder mHolder;
    private SurfaceHolder mSurfaceHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        SurfaceView mSurfaceView = (SurfaceView)findViewById(R.id.surface);
        mSurfaceHolder = mSurfaceView.getHolder();
    }

    public SurfaceHolder getSurfaceHolder(){
        return mSurfaceHolder;
    }
}
