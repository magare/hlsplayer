package com.rtrk.android.master.tester;

import java.util.logging.Logger;
import android.util.Log;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.test.ActivityInstrumentationTestCase2;
import android.view.SurfaceView;
import android.view.Surface;

import com.rtrk.android.master.player.Player;

import com.rtrk.android.master.player.Player;
import com.google.android.exoplayer.ExoPlaybackException;


public class HLSTestClass extends ActivityInstrumentationTestCase2<MediaStubActivity>{
    private String TAG = "HLSTestClass";
    protected Context mContext;
    protected Player mPlayer = null;
    protected MediaStubActivity mActivity;
    protected boolean mPlayerReady = false;
    private Player.Callback mPlayerCallback = new Player.Callback(){
        private boolean mFirstFrameDrawn = false;

        @Override
        public void onPrepared(){
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int state){
            mPlayerReady = (state == Player.STATE_READY);
        }

        @Override
        public void onPlayWhenReadyCommitted(){

        }

        @Override
        public void onPlayerError(ExoPlaybackException e){
            e.printStackTrace();
            fail();
        }

        @Override
        public void onDrawnToSurface(Surface surface){
            mFirstFrameDrawn = true;
        }

        @Override
        public void onText(String text){

        }
    };

    public HLSTestClass(){
        super(MediaStubActivity.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        mActivity = getActivity();
        getInstrumentation().waitForIdleSync();

        Log.d(TAG, "setUp()");
        try{
            runTestOnUiThread(new Runnable() {
                public void run(){
                    mPlayer = new Player();
                    assertEquals(true, mPlayer != null);
                }

            });
        }catch(Throwable e){
            e.printStackTrace();
            fail();
        }

        mContext = getInstrumentation().getTargetContext();
    }

    @Override
    protected void tearDown() throws Exception{
        if(mPlayer != null){ /* if player not released, release it*/
            mPlayer.release();
            mPlayer = null;
        }

        mActivity = null;
        super.tearDown();
    }

    public void testHLS(){
        int seconds = 0;
        Log.d(TAG, "Test started");

        try{
            mPlayer.setSurface(mActivity.getSurfaceHolder().getSurface());
            mPlayer.setVolume(50);
            mPlayer.setPlayWhenReady(true);
            mPlayer.addCallback(mPlayerCallback);
            //mPlayer.prepare(mContext, Uri.parse("http://tv.life.ru/lifetv/720p/index.m3u8"), Player.SOURCE_TYPE_HLS);
            mPlayer.prepare(mContext, Uri.parse("http://daserste_live-lh.akamaihd.net/i/daserste_de@91204/master.m3u8"),
                        Player.SOURCE_TYPE_HLS);

            Log.d(TAG, "Waiting for player to get ready...");
            while(mPlayerReady == false){
                if(seconds++ >= 5){
                    fail();
                }
                Thread.sleep(1000);
            }

            Log.d(TAG, "Waiting...");
            seconds = 0;
            while(seconds++ < 10){
                Thread.sleep(1000);
                assertEquals(mPlayerReady, true);
            }

            mPlayer.stop();
        }catch(Exception e){
            e.printStackTrace();
            fail();
        }
    }
}
