package com.rtrk.android.master.player;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.MediaCodec;
import android.media.tv.TvTrackInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
//import android.telecom.Call;
import android.view.Surface;
import android.util.Log;

import java.lang.String;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.android.exoplayer.DefaultLoadControl;
import com.google.android.exoplayer.DummyTrackRenderer;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.ExoPlayerLibraryInfo;
import com.google.android.exoplayer.LoadControl;
import com.google.android.exoplayer.MediaCodecAudioTrackRenderer;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecUtil;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.SampleSource;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.chunk.ChunkSampleSource;
import com.google.android.exoplayer.chunk.ChunkSource;
import com.google.android.exoplayer.chunk.Format;
import com.google.android.exoplayer.chunk.FormatEvaluator;
import com.google.android.exoplayer.chunk.MultiTrackChunkSource;
import com.google.android.exoplayer.dash.DashChunkSource;
import com.google.android.exoplayer.dash.mpd.AdaptationSet;
import com.google.android.exoplayer.dash.mpd.MediaPresentationDescription;
import com.google.android.exoplayer.dash.mpd.MediaPresentationDescriptionParser;
import com.google.android.exoplayer.dash.mpd.Period;
import com.google.android.exoplayer.dash.mpd.Representation;
import com.google.android.exoplayer.hls.HlsChunkSource;
import com.google.android.exoplayer.hls.HlsPlaylist;
import com.google.android.exoplayer.hls.HlsPlaylistParser;
import com.google.android.exoplayer.hls.HlsSampleSource;
import com.google.android.exoplayer.mp4.Track;
import com.google.android.exoplayer.source.DefaultSampleSource;
import com.google.android.exoplayer.source.FrameworkSampleExtractor;
import com.google.android.exoplayer.text.TextRenderer;
import com.google.android.exoplayer.text.eia608.Eia608TrackRenderer;
import com.google.android.exoplayer.upstream.BufferPool;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DataSpec;
import com.google.android.exoplayer.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer.upstream.UriDataSource;
import com.google.android.exoplayer.util.ManifestFetcher;
import com.google.android.exoplayer.util.MimeTypes;
import com.google.android.exoplayer.util.Util;

public class Player implements TextRenderer{
    private static final String TAG = "Player";
    public static final int SOURCE_TYPE_HLS = 1;

    private static final int MIN_BUFFER_MS = 2000;
    private static final int MIN_REBUFFER_MS = 7000;

    public static final int STATUS_SEEK_SUCCESS = 1;
    public static final int STATUS_SEEK_WAITING = 2;
    public static final int STATUS_SEEK_UNKNOWN_TIME = 3;

    public static final int STATE_IDLE = ExoPlayer.STATE_IDLE;
    public static final int STATE_READY = ExoPlayer.STATE_READY;
    public static final int STATE_BUFFERING = ExoPlayer.STATE_BUFFERING;
    //public static final int STATE_BUFFERING = ExoPlayer.STATE_BUFFERING;

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int VIDEO_BUFFER_SEGMENTS = 200;
    private static final int AUDIO_BUFFER_SEGMENTS = 60;
    private static final int LIVE_EDGE_LATENCY_MS = 30000;

    private final int RENDERER_COUNT = 2;
    private final int NO_TRACK_SELECTED = -1;

    private Handler mHandler;
    private final ExoPlayer mPlayer;
    private TrackRenderer mAudioRenderer;
    private TrackRenderer mVideoRenderer;
    private TrackRenderer mTextRenderer;
    private Surface mSurface;
    private Long mPendingSeekPosition;
    private float mVolume;
    private final TvTrackInfo[][] mTvTracks = new TvTrackInfo[RENDERER_COUNT][];
    private final int[] mSelectedTvTracks = new int[RENDERER_COUNT];
    private final MultiTrackChunkSource[] mMultiTrackSources = new MultiTrackChunkSource[RENDERER_COUNT];
    private final CopyOnWriteArrayList<Callback> mCallbacks;

    private Uri mUri;
    private Context mContext;

    /* Client defined callbacks to be fired on player specific events */
    public interface Callback{
        void onPrepared();
        void onPlayerStateChanged(boolean playWhenReady, int state);
        void onPlayWhenReadyCommitted();
        void onPlayerError(ExoPlaybackException e);
        void onDrawnToSurface(Surface surface);
        void onText(String text);
    }

    private final MediaCodecVideoTrackRenderer.EventListener mVideoRendererEventListener =
        new MediaCodecVideoTrackRenderer.EventListener() {
            @Override
            public void onDroppedFrames(int count, long elapsed){
                Log.w(TAG, "VideoRenderer.onDroppedFrames");
            }

            @Override
            public void onVideoSizeChanged(int width, int height, float pixelWidthHeightRatio){
                Log.w(TAG, "VideoRenderer.onVideoSizeChanged");

            }

            @Override
            public void onDrawnToSurface(Surface surface){
                Log.d(TAG, "VideoRenderer.onDrawnToSurface");

                for(Callback callback : mCallbacks){
                    callback.onDrawnToSurface(surface);
                }
            }

            @Override
            public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e){
                Log.e(TAG, "VideoRenderer.onDecodedInitializationError");

                for(Callback callback : mCallbacks){
                    callback.onPlayerError(new ExoPlaybackException(e));
                }
            }

            @Override
            public void onCryptoError(MediaCodec.CryptoException e){
                Log.e(TAG, "VideoRenderer.onCryptoError");

                for(Callback callback : mCallbacks){
                    callback.onPlayerError(new ExoPlaybackException(e));
                }
            }
        };

    /* Method name: isPlayerPrepared
     * For internal use. Used to determine whether ExoPlayer instance is ready to immediatelly play from source
     * ExoPlayer's method getPlaybackState() returns:
     * STATE_IDLE       - ExoPlayer is neither ready nor being prepaired. It is waiting for action.
     * STATE_PREPARING  - Player is prepared
     * STATE_BUFFERING  - Player is prepared but not able to immediatelly play from the current position
     * STATE_READY      - Player is prepared and able to play immediatelly from the current position
     * STATE_ENDED      - Player has finished playing the media
     */
    private static boolean isPlayerPrepared(ExoPlayer player){
        int state = player.getPlaybackState();
        return (state != ExoPlayer.STATE_PREPARING && state != ExoPlayer.STATE_IDLE);
    }

    /* Method name: seekTo
     * For external use, when client wants to seek for position in media to play.
     * Not supported for HLS
     */
    public int seekTo(long position){
        boolean state = isPlayerPrepared(mPlayer);
        int result = STATUS_SEEK_UNKNOWN_TIME;

        if(state){
            if(mPlayer.getDuration() != ExoPlayer.UNKNOWN_TIME){
                mPlayer.seekTo(position);
                result = STATUS_SEEK_SUCCESS;
            }
        } else {
            mPendingSeekPosition = position;
            result = STATUS_SEEK_WAITING;
        }

        return result;
    }

    private void prepareInternal(){
        mPlayer.prepare(mAudioRenderer, mVideoRenderer);
        mPlayer.sendMessage(mAudioRenderer, MediaCodecAudioTrackRenderer.MSG_SET_VOLUME, mVolume);
        mPlayer.sendMessage(mVideoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, mSurface);
        //mPlayer.setRendererEnabled(TvTrackInfo.TYPE_SUBTITLE, false); // disabe subtitles

        for(Callback callback : mCallbacks){
            callback.onPrepared();
        }
    }

    public void setVolume(float volume){
        mVolume = volume;
        if(mPlayer != null && mAudioRenderer != null){
            mPlayer.sendMessage(mAudioRenderer, MediaCodecAudioTrackRenderer.MSG_SET_VOLUME, volume);
        }
    }

    public void setSurface(Surface surface){
        mSurface = surface;
        if(mPlayer != null && mVideoRenderer != null){
            mPlayer.sendMessage(mVideoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, surface);
        }
    }

    public void setPlayWhenReady(boolean playWhenReady){
        mPlayer.setPlayWhenReady(playWhenReady);
    }

    public void stop(){
        mPlayer.stop();
    }

    public void release(){
        mPlayer.release();
    }

    public void addCallback(Callback callback){
        mCallbacks.add(callback);
    }

    public void removeCallback(Callback callback){
        mCallbacks.remove(callback);
    }

    public void prepare(Context context, final Uri uri, int playbackType){
        Log.d(TAG, "prepare()");
        mContext = context;
        mUri = uri;

        if(playbackType == SOURCE_TYPE_HLS){
            Log.d(TAG, "HLS playback type");

            final String userAgent = getUserAgent(context);
            HlsPlaylistParser parser = new HlsPlaylistParser();
            ManifestFetcher<HlsPlaylist> playlistFetcher = new ManifestFetcher<>(parser,
                                                                                uri.toString(),
                                                                                uri.toString(),
                                                                                "bla");
            playlistFetcher.singleLoad(mHandler.getLooper(),
                    new ManifestFetcher.ManifestCallback<HlsPlaylist>(){
                        @Override
                        public void onManifest(String contentId, HlsPlaylist manifest){
                            Log.d(TAG, "ManifestFetcher.singleLoad()-> onManifest() callback");

                            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                            DataSource dataSource = new UriDataSource(userAgent, bandwidthMeter);
                            HlsChunkSource chunkSource = new HlsChunkSource(dataSource,
                                                                            uri.toString(),
                                                                            manifest,
                                                                            bandwidthMeter,
                                                                            null,
                                                                            HlsChunkSource.ADAPTIVE_MODE_SPLICE);
                            HlsSampleSource sampleSource = new HlsSampleSource(chunkSource, true, 2);

                            mAudioRenderer = new MediaCodecAudioTrackRenderer(sampleSource);
                            mVideoRenderer = new MediaCodecVideoTrackRenderer(sampleSource,
                                                                              MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT,
                                                                              0,
                                                                              mHandler,
                                                                              mVideoRendererEventListener,
                                                                              50);
                            prepareInternal();
                        }

                        @Override
                        public void onManifestError(String contentId, IOException e){
                            Log.e(TAG, "onManifest Error callback from ExoPlayer");

                            for(Callback callback : mCallbacks){
                                callback.onPlayerError(new ExoPlaybackException(e));
                            }
                        }
                    });
        } else {
            /* FOR DIFFERENT PLAYBACK TYPES. THIS ASSIGEMENT IS FOR HLS ONLY.*/
        }
    }

    @Override
    public void onText(String text){
        for(Callback callback : mCallbacks){
            callback.onText(text);
        }
    }

    private static String getUserAgent(Context context){
        String versionName;

        try{
            String packageName = context.getPackageName();
            PackageInfo info = context.getPackageManager().getPackageInfo(packageName, 0);
            versionName = info.versionName;
        } catch(PackageManager.NameNotFoundException e){
            versionName = "?";
        }

        return "TifWithExoPlayer/" + versionName + "(Linux;Android " + Build.VERSION.RELEASE + ") "
            + "ExoPlayerLib/" + ExoPlayerLibraryInfo.VERSION;
    }

    public Player(){
        Log.d(TAG, "Player()");

        mHandler = new Handler();
        for(int i = 0; i < RENDERER_COUNT; i++){
            mTvTracks[i] = new TvTrackInfo[0];
            mSelectedTvTracks[i] = NO_TRACK_SELECTED;
        }

        mCallbacks = new CopyOnWriteArrayList<>();
        mPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT, MIN_BUFFER_MS, MIN_REBUFFER_MS);
        mPlayer.addListener(new ExoPlayer.Listener(){
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState){
                Log.d(TAG, "onPlayerStateChanged()");

                for(Callback callback : mCallbacks){
                    callback.onPlayerStateChanged(playWhenReady, playbackState);
                }

                if(mPendingSeekPosition != null && seekTo(mPendingSeekPosition) == STATUS_SEEK_SUCCESS){
                    mPendingSeekPosition = null;
                }
            }

            @Override
            public void onPlayWhenReadyCommitted(){
                for(Callback callback : mCallbacks){
                    callback.onPlayWhenReadyCommitted();
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException e){
                for(Callback callback : mCallbacks){
                    callback.onPlayerError(e);
                }
            }
        });
    }
}
