
package com.rtrk.android.master.player;

import android.accounts.Account;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.SyncStatusObserver;
import android.media.tv.TvContract;
import android.media.tv.TvInputInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v17.leanback.widget.Action;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.DetailsOverviewRow;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.rtrk.android.master.R;
import com.rtrk.android.master.syncadapter.DummyAccountService;
import com.rtrk.android.master.syncadapter.SyncUtils;
import com.rtrk.android.master.utils.FeedUtil;
import com.rtrk.android.master.utils.TvContractUtils;
import com.rtrk.android.master.xmltv.XmlTvParser;

/**
 * The setup activity for demonstrating {@link BasicTvInputService}.
 */
public class BasicTvInputSetupActivity extends Activity {

    private static final String TAG = "BasicTvInputSetupActivity";

    public static final int SYNC_UPDATE_MS = 1000;

    private XmlTvParser.TvListing mTvListing = null;

    private String mInputId = null;

    private Button mAddChennels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_setup);

        mInputId = getIntent().getStringExtra(TvInputInfo.EXTRA_INPUT_ID);
        Log.d(TAG, "input id: " + mInputId);

        mAddChennels = (Button) findViewById(R.id.add_channels);
    }

    public void onClickAddChannels(View view) {
        mAddChennels.setText(R.string.basic_tif_setup_in_progress);
        new SetupTask().execute();
    }

    public void onClickCancelSetup(View view) {
        finish();
    }

    private class SetupTask extends AsyncTask<Uri, String, Boolean> {
        @Override
        protected Boolean doInBackground(Uri... params) {
            // Get channels
            mTvListing = FeedUtil.getTvListings(getApplicationContext(), mInputId);
            setupChannels(mInputId);
            return true;
        }
    }

    private void onError(int errorResId) {
        Toast.makeText(getApplicationContext(), errorResId, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void setupChannels(String inputId) {
        if (mTvListing == null) {
            onError(R.string.feed_error_message);
            return;
        }
        TvContractUtils.updateChannels(getApplicationContext(), inputId, mTvListing.channels);
        SyncUtils.setUpPeriodicSync(getApplicationContext(), inputId);
        SyncUtils.requestSync(inputId, true); // force sync current program

        Thread fullSync = new Thread() {
            private boolean mSyncServiceStarted = false;
            private boolean mFinished = false;

            @Override
            public void run() {
                while (!mFinished) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Account account = DummyAccountService
                                    .getAccount(SyncUtils.ACCOUNT_TYPE);
                            boolean syncActive = ContentResolver.isSyncActive(account,
                                    TvContract.AUTHORITY);
                            boolean syncPending = ContentResolver.isSyncPending(account,
                                    TvContract.AUTHORITY);
                            boolean syncServiceInProgress = syncActive || syncPending;
                            Log.d(TAG, "[onStatusChanged][" + mSyncServiceStarted + " "
                                    + syncServiceInProgress + "]");

                            if (mSyncServiceStarted && !syncServiceInProgress) {
                                // Only current programs are registered at this
                                // point. Request a full sync.
                                Log.d(TAG,
                                        "[onStatusChanged][Sync not in progress, perform full program sync]");
                                SyncUtils.requestSync(mInputId, false);

                                setResult(Activity.RESULT_OK);
                                finish();
                                mFinished = true;
                            }
                            if (!mSyncServiceStarted && syncServiceInProgress) {
                                mSyncServiceStarted = syncServiceInProgress;
                            }
                        }
                    });
                    try {
                        Thread.sleep(SYNC_UPDATE_MS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        fullSync.start();
    }
}
