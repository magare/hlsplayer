
package com.rtrk.android.master.player;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.tv.TvContentRating;
import android.media.tv.TvInputManager;
import android.media.tv.TvInputService;
import android.media.tv.TvTrackInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.accessibility.CaptioningManager;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.ExoPlaybackException;

import com.rtrk.android.master.R;
import com.rtrk.android.master.syncadapter.SyncUtils;
import com.rtrk.android.master.utils.PlaybackInfo;
import com.rtrk.android.master.utils.TvContractUtils;
import com.rtrk.android.master.player.Player;
import com.rtrk.android.master.data.Program;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * TvInputService which provides a full implementation of EPG, subtitles,
 * multi-audio, parental controls, and overlay view.
 */
public class BasicTvInputService extends TvInputService {
    private static final String TAG = "BasicTvInputService";

    public static final int ANDROID_MEDIA_PLAYER = 1;

    private HandlerThread mHandlerThread;
    private Handler mDbHandler;

    private List<BasicTvInputSessionImpl> mSessions;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandlerThread = new HandlerThread(getClass().getSimpleName());
        mHandlerThread.start();
        mDbHandler = new Handler(mHandlerThread.getLooper());

        setTheme(android.R.style.Theme_Holo_Light_NoActionBar);

        mSessions = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandlerThread.quit();
        mHandlerThread = null;
        mDbHandler = null;
    }

    @Override
    public final Session onCreateSession(String inputId) {
        BasicTvInputSessionImpl session = new BasicTvInputSessionImpl(this, inputId);
        session.setOverlayViewEnabled(true);
        mSessions.add(session);
        return session;
    }

    class BasicTvInputSessionImpl extends TvInputService.Session implements Handler.Callback {
        private static final int MSG_PLAY_PROGRAM = 1000;

        private final Context mContext;
        private final String mInputId;
        private final TvInputManager mTvInputManager;
        private Surface mSurface;
        private float mVolume;

        private PlaybackInfo mCurrentPlaybackInfo;
        private TvContentRating mLastBlockedRating;
        private TvContentRating mCurrentContentRating;

        private boolean mEpgSyncRequested;
        private final Set<TvContentRating> mUnblockedRatingSet = new HashSet<>();
        private final Handler mHandler;

        private PlayCurrentProgramRunnable mPlayCurrentProgramRunnable;

        /* Here goes Player based on ExoPlayer */
        private Player mPlayer;

        public Surface getSurface(){
            return mSurface;
        }

        private final Player.Callback mPlayerCallback = new Player.Callback(){
            private boolean mFirstFrameDrawn = false;

            @Override
            public void onPrepared(){
                Log.d(TAG, "onPrepared()");

                mFirstFrameDrawn = false;
                //List<TvTrackInfo> tracks = new ArrayList();

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState){
                Log.d(TAG, "onPlayerStateChanged()");

                if(playWhenReady == true && playbackState == ExoPlayer.STATE_BUFFERING){
                    if(mFirstFrameDrawn == true){
                        notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_BUFFERING);
                    }
                }else if(playWhenReady && playbackState == ExoPlayer.STATE_READY){
                    notifyVideoAvailable();
                }
            }

            @Override
            public void onPlayWhenReadyCommitted(){

            }

            @Override
            public void onPlayerError(ExoPlaybackException e){
            }

            @Override
            public void onDrawnToSurface(Surface surface){
                mFirstFrameDrawn = true;
                notifyVideoAvailable();

            }

            @Override
            public void onText(String text){

            }
        };

        protected BasicTvInputSessionImpl(Context context, String inputId) {
            super(context);

            mContext = context;
            mInputId = inputId;
            mTvInputManager = (TvInputManager) context.getSystemService(Context.TV_INPUT_SERVICE);
            mLastBlockedRating = null;
            mHandler = new Handler(this);
        }

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == MSG_PLAY_PROGRAM) {
                playProgram((Program) msg.obj);
                return true;
            }
            return false;
        }

        @Override
        public void onRelease() {
            if (mDbHandler != null) {
                mDbHandler.removeCallbacks(mPlayCurrentProgramRunnable);
            }
            releasePlayer();
            mSessions.remove(this);
        }

        @Override
        public boolean onSetSurface(Surface surface) {
            // ! TODO, pass surface to Media Player
            Log.d(TAG, "Surface SET");
            if (mPlayer != null) {
                mPlayer.setSurface(surface);
            }
            mSurface = surface;
            return true;
        }

        @Override
        public void onSetStreamVolume(float volume) {
            // ! TODO, set volume to Media Player
            if (mPlayer != null) {
                mPlayer.setVolume(volume);
            }
            mVolume = volume;
        }

        private boolean playProgram(Program info) {
            Log.i(TAG, info.toString());
            releasePlayer();

            //Pair<Integer, String> videoInfo = TvContractUtils.parseProgramInternalProviderData(info.getInternalProviderData());

            // ! Create new player

                mPlayer = new Player();
                mPlayer.addCallback(mPlayerCallback);
                mPlayer.prepare(BasicTvInputService.this, TvContractUtils.getProgramSourceUri(info.getInternalProviderData()), Player.SOURCE_TYPE_HLS);
                mPlayer.setSurface(mSurface);
                mPlayer.setVolume(mVolume);



            //int id = getResources().getIdentifier("raw/" + info.videoUrl, "raw", getPackageName());
            //AssetFileDescriptor afd = getResources().openRawResourceFd(id);
            //if (afd == null) {
            //    return false;
            //}

            Log.d(TAG, "calling player.prepare");


            long nowMs = System.currentTimeMillis();
          /*  int seekPosMs = (int) (nowMs - info.getStartTimeUtcMillis());
            if (seekPosMs > 0) {
                mPlayer.seekTo(seekPosMs);
            }*/
            mPlayer.setPlayWhenReady(true);
           // mDbHandler.postDelayed(mPlayCurrentProgramRunnable,
             //       info.getEndTimeUtcMillis() - nowMs + 1000);
            return true;
        }

        @Override
        public boolean onTune(Uri channelUri) {
            Log.i(TAG, "[onTune][" + channelUri + "]");
            notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING);
            mUnblockedRatingSet.clear();

            mDbHandler.removeCallbacks(mPlayCurrentProgramRunnable);
            mPlayCurrentProgramRunnable = new PlayCurrentProgramRunnable(channelUri);
            mDbHandler.post(mPlayCurrentProgramRunnable);
            return true;
        }

        @Override
        public void onSetCaptionEnabled(boolean enabled) {
            // ! Not supported at the moment
        }

        @Override
        public boolean onSelectTrack(int type, String trackId) {
            // ! Not supported at the moment
            return false;
        }

        @Override
        public void onUnblockContent(TvContentRating rating) {
            // ! Not supported at the moment
        }

        // ! TODO deinit player
        private void releasePlayer() {
            if (mPlayer != null) {
                mPlayer.removeCallback(mPlayerCallback);
                mPlayer.setSurface(null);
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
            }
        }

        private class PlayCurrentProgramRunnable implements Runnable {
            private static final int RETRY_DELAY_MS = 2000;
            private final Uri mChannelUri;

            public PlayCurrentProgramRunnable(Uri channelUri) {
                mChannelUri = channelUri;
            }

            @Override
            public void run() {
                ContentResolver resolver = mContext.getContentResolver();
                Program program = TvContractUtils.getCurrentProgram(resolver, mChannelUri);
                if (program != null) {
                    mHandler.removeMessages(MSG_PLAY_PROGRAM);
                    mHandler.obtainMessage(MSG_PLAY_PROGRAM, program).sendToTarget();
                } else {
                    Log.w(TAG, "Failed to get program info for " + mChannelUri + ". Retry in "
                            + RETRY_DELAY_MS + "ms.");
                    mDbHandler.postDelayed(mPlayCurrentProgramRunnable, RETRY_DELAY_MS);
                    if (!mEpgSyncRequested) {
                        SyncUtils.requestSync(mInputId, true);
                        mEpgSyncRequested = true;
                    }
                }
            }
        }
    }
}
